# Bachelor Thesis: A game-based platform for synthesis of programs controlling railway systems

A game-based platform should be developed for generation of programs that can 
control railway systems. These programs should be correct by construction. 